# Interesting Services

* [QCard](https://qcard.link/)
    * qr code contact cards with all data in the url anchor in the code url
* [report-uri](https://report-uri.com/)
    * simple app for receiving csp reports, dmarc reports, etc